function [ phaseCorrPts ] = phaseCorrection(phaseNoisePts)
%PHASECORRECTION Summary of this function goes here
%   Detailed explanation goes here
    
    phaseCorrPts = zeros(size(phaseNoisePts));
    phases = zeros(size(phaseNoisePts,1),1);
    
    for j = 1:size(phaseNoisePts,1)
        phases(j) = angle(phaseNoisePts(j));
    end
    
    corrPhase = mean(phases);
    
    for j = 1:size(phaseNoisePts,1)
        phaseCorrPts(j) = abs(phaseNoisePts(j))*exp(corrPhase);
    end

end

