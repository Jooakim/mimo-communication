function [ blockPts, filterCoeff] = fadingCorrection( blockPts, refPts, nbrOfPilots )
%FADINGCORRECTION corrects 
% Solves a minimum mean square problem to find the correction constants
% to a filter array

    k = 2;
    x = zeros(nbrOfPilots,1);
    y = zeros(nbrOfPilots, k);
    
    for j = 1:nbrOfPilots
        x(j) = refPts(mod(j-1,length(refPts))+1);
    end

    % Construct the y matrix
    if length(blockPts) >= nbrOfPilots
        for j = 1:nbrOfPilots
            for l = 1:k
                if j-l+1 > 0
                    y(j,l) = blockPts(j-l+1);
                else
                    y(j,l) = 0;
                end
            end
        end
    else
         for j = 1:length(blockPts)
            for l = 1:k
                if j-l+1 > 0
                    y(j,l) = blockPts(j-l+1);
                else
                    y(j,l) = 0;
                end
            end
         end
    end
    
    % Calculate filter coefficients
    g = pinv(y)*x;

    % Calculate approximate pts
    blockPts = filter(g, 1, blockPts);
    
    filterCoeff = abs(g).^2;
    
    
%     size(filterCoeff);
end

                