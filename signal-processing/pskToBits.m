function [ bits ] = pskToBits( data )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

bits = zeros(2*length(data),1);

points = [-1+1i -1-1i 1+1i 1-1i]/sqrt(2);
indexForBit = 1;
for j = 1:length(data);
    
    [notImportant ,indexOfCorr] = min(abs(points-data(j)));
    
    if indexOfCorr == 1
        bits(indexForBit) = 1;
        bits(indexForBit+1) = 0;
    elseif indexOfCorr == 2
        bits(indexForBit) = 1;
        bits(indexForBit+1) = 1;
        
    elseif indexOfCorr == 3
        bits(indexForBit) = 0;
        bits(indexForBit+1) = 0;
        
    else
        bits(indexForBit) = 0;
        bits(indexForBit+1) = 1;
    end
    indexForBit = indexForBit + 2;
end


end

