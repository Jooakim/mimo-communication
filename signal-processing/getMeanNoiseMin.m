clf;
smallestValue = min(min(meanNoises(25:end,1:90)))
[row, col] = find(meanNoises == smallestValue);
v(1:length(row),1) = smallestValue;
hold on
surf(meanNoises(:,1:133))
plot3(col, row, v,'r.','markersize',50)