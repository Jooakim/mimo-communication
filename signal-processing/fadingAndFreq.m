function [ corrPts, meanCorrAngle, meanPilotAngle, meanFilterCoeff ] = fadingAndFreq( signal, index, blockSize, points )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    if index < 0
        index = index + blockSize;
    end
    %Variables
    meanCorrAngle = 0;
    meanPilotAngle = 0;
    meanFilterCoeff = 0;
    
    corrPts = signal;
    for j = 1:blockSize:length(signal)
        if j + blockSize < length(signal)
            [corrPts(j:j+blockSize-1), corrAngle, pilotAngle] = frequencyCorrection(signal(j:j+blockSize-1), points, length(points));
            [corrPts(j:j+blockSize-1), filterCoeff] = fadingCorrection(corrPts(j:j+blockSize-1), points, length(points));
        else 
            [corrPts(j:end), corrAngle, pilotAngle]= frequencyCorrection(signal(j:end), points, length(points));
            [corrPts(j:end), filterCoeff] = fadingCorrection(corrPts(j:end), points, length(points));
        end
        meanCorrAngle = meanCorrAngle + corrAngle;
        meanPilotAngle = meanPilotAngle + pilotAngle;
        meanFilterCoeff = meanFilterCoeff + filterCoeff;
    end
    
    % Get the average correction
    meanCorrAngle = meanCorrAngle/ceil(length(signal)/blockSize);
    meanPilotAngle = meanPilotAngle/ceil(length(signal)/blockSize); 
    meanFilterCoeff = meanFilterCoeff/ceil(length(signal)/blockSize);
    

    
%     corrPts = frequencyCorrection(signal, points, length(points));
%     corrPts = fadingCorrection(corrPts, points, length(points));
 
    %corrPts = signal;

end

