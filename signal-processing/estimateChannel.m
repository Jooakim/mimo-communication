function [ corrPhase, corrPhaseDeg ] = estimateChannel( recdSignal, s1, s2 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    % Test av frekvenskorrektion
    hold on
    
    transmSymbols = [s1(1:min(length(s1), length(s2))); s2(1:min(length(s1), length(s2)))];
    
    [Rxy1, lags] = xcorr(recdSignal, s1);
    
    subplot(2,1,1)
    hold on
    plot(lags,abs(Rxy1))
    [hej, index1] = find(abs(Rxy1) == max(abs(Rxy1)))
    plot(index1-max(lags)-1, max(abs(Rxy1)), 'g*')
    while index1 - max(lags) - 1 - length(s1)/2 > 0
        index1 = index1 - length(s1)/2; %QPSK tv� bitar per symbol
        plot(index1-max(lags)-1, max(abs(Rxy1)), 'r*')
    end
    index1 = index1 - max(lags);
    legend(num2str(index1))
%     
    [Rxy2, lags] = xcorr(recdSignal, s2);
    
    subplot(2,1,2)
    hold on
    plot(lags,abs(Rxy2))
    [hej, index2] = find(abs(Rxy2) == max(abs(Rxy2)))
    plot(index2-max(lags)-1, max(abs(Rxy2)), 'g*')
    while index2 - max(lags) - 1 - length(s1)/2 > 0
        index2 = index2 - length(s1)/2; %QPSK tv� bitar per symbol
        plot(index2-max(lags)-1, max(abs(Rxy2)), 'r*')
    end
    index2 = index2 - max(lags);
    legend(num2str(index2))
    
    if max(abs(Rxy2)) > max(abs(Rxy1))
        [index, recdSignal]  = detectPilots( recdSignal, transmSymbols(2,:));
    else
        [index, recdSignal]  = detectPilots( recdSignal, transmSymbols(1,:));
    end
    
    transmSymbols = conj(transmSymbols');
%     recdSignal2 = recdSignal(length(transmSymbols)+1:2*length(transmSymbols));
    if length(transmSymbols) < length(recdSignal)
        recdSignal = recdSignal(1:length(transmSymbols));
    else
        temp1 = transmSymbols(1:length(recdSignal),1);
        temp2 = transmSymbols(1:length(recdSignal),2);
        transmSymbols = [temp1 temp2];
        %transmSymbols = temp1;
    end
    channel = pinv(transmSymbols)*conj(recdSignal');

    v1 = angle(channel(1));
    v2 = angle(channel(2));
    
    corrPhase = - v1 + v2;
    corrPhaseDeg = corrPhase*pi/180;
end