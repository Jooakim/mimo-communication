function [  ] = bitsToPilots(bits, nbrOfPilots )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    indexOfBit = 1;
    
    points = [-1+1i -1-1i 1+1i 1-1i]/sqrt(2);
    fid = fopen('..\Signalbehandling-LabVIEW\Tx\Pilots.txt','w');
    myformat = '%5.4g + %5.4gi, ';

    for j = 1:nbrOfPilots
        
        if bits(indexOfBit) == 1 && bits(indexOfBit+1) == 0
            indexOfPilot = 1;
        elseif bits(indexOfBit) == 1 && bits(indexOfBit+1) == 1
            indexOfPilot = 2;  
        elseif bits(indexOfBit) == 0 && bits(indexOfBit+1) == 0
            indexOfPilot = 3;
        else
            indexOfPilot = 4;
        end
        fprintf(fid, myformat, [real(points(indexOfPilot)) imag(points(indexOfPilot))]);
    indexOfBit = indexOfBit + 2;
    end
    fclose(fid);
    
   


end

