function snr = getSNR( signal, pilots)
%GETSNR returns the signal to noise ratio 
% FUNKAR INTE BRA MED MATLAB F�R TILLF�LLET
% TY PILOTS F�R KORT
%     clf
%     subplot(2,1,1)
%     [corr, lags] = xcorr(signal, pilots);
%     plot(lags,abs(corr))
%     str = num2str(length(signal));
%     str2 = num2str(length(pilots));
%     legend(strcat('signal length: ',str));
    [index, signal] = detectPilots(signal, pilots);
    
    if length(signal) < length(pilots)
        pilots = pilots(1:length(signal));
    else
        signal = signal(1:length(pilots));
    end
    
%     pilotAmp = sum(abs(pilots));
%     signalAmp = sum(abs(signal));
%     ampNorm = signalAmp/pilotAmp;
%     pilots = ampNorm*pilots;
    [corr, lags] = xcorr(signal, pilots);
%     subplot(2,1,2)
%     
%     plot(lags,abs(corr))
%     legend(strcat('pilot length: ',str2));

    [value, index] = find(abs(corr) == max(abs(corr)));
    Rxy = max(abs(corr));
    k = Rxy/(sum(abs(pilots).^2))
    w = abs(signal) - abs(k*pilots);
    P_x = sum(abs(signal).^2);
    P_w = sum(abs(w).^2);
    snr = 10*log10(P_x/P_w);

    
    hold on
%     plot(signal(1:100), '*');
%     plot(k*pilotSeq(1:100), '*g')
end

