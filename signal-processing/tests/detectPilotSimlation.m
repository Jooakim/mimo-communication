clear all
close all
clc 
clf

% ------------ Variables ------------
% To set
blockSize = 100; 
nbrOfPilots = 10;
%samplingRate = 16;
nbrOfSimulations = 1e3;
points = [-1+1i -1-1i 1+1i 1-1i];

% Don't touch
nbrBitErr = 0;
corrPts = zeros(nbrOfSimulations, 1);
transmPts = zeros(nbrOfSimulations,1);
nbrOfBlocks = nbrOfSimulations/blockSize;

% ------------ Transmit Points ------------
rndIndexVector = ceil(length(points)*rand(nbrOfSimulations, 1));
for j = 1:nbrOfBlocks
    transmPts((j-1)*blockSize+1:j*blockSize) = transmitBlock(points, rndIndexVector, nbrOfPilots, blockSize, j);
end
transmPts = [transmPts; transmPts; transmPts; transmPts; transmPts; transmPts; transmPts; transmPts; transmPts; transmPts];

recdPts = channel(transmPts, 64+16+8+4);

detectPilots(recdPts, transmPts(1:30));
hold on


