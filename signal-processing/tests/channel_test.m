clear all
close all
clc 
clf

% ------------ Variables ------------
% To set
blockSize = 100; 
nbrOfPilots = 12;
samplingRate = 16;
nbrOfSimulations = 100;
points = [-1+1i -1-1i 1+1i 1-1i];

% Don't touch
nbrBitErr = 0;
corrPts = zeros(nbrOfSimulations, 1);
transmPts = zeros(nbrOfSimulations,1);
nbrOfBlocks = nbrOfSimulations/blockSize;
rndIndexVector = ceil(length(points)*rand(nbrOfSimulations, 1));

% ------------ Transmit Points ------------
for j = 1:nbrOfBlocks
    transmPts((j-1)*blockSize+1:j*blockSize) = transmitBlock(points, rndIndexVector, nbrOfPilots, blockSize, j);
end
pts = channel(transmPts, 64);
resampledPts = resample(pts, samplingRate, 1);
% resampledPts = channel(resampledPts, 32);
hold on
plot([-2 2],[0 0], 'k');
plot([0 0],[-2 2], 'k'); 
axis([-2 2 -2 2])
plot(resampledPts)
plot(points, 'r.', 'markersize', 20)
figure
% ------------ Simulate errors ------------
% Modes turned on are specified by bitget on last param.
recdNoisyPts = channel(resampledPts, 0);

plotEyeDiagram(recdNoisyPts, samplingRate); % plots an eye diagram

% ------------ Sample points ------------
[sampledPts, ~] = gardnerSync(recdNoisyPts, samplingRate, -1);
% sampledPts = downsample(recdNoisyPts, samplingRate);

% ------------ Correct errors ------------
%sampledPts = sampledPts*exp(1i*pi/2);
%index = detectPilots(sampledPts, transmPts(20:40))
% corrPts = sampledPts
for j = 1:blockSize:length(sampledPts)
    corrPts(j:j+blockSize-1) = frequencyCorrection(sampledPts(j:j+blockSize-1), points, nbrOfPilots);
    corrPts(j:j+blockSize-1) = fadingCorrection(corrPts(j:j+blockSize-1), points, nbrOfPilots);
end
corrPts = sampledPts

% ------------ Plot recieved points ------------ 
figure
hold on
% plots coordinate system
plot([-2 2],[0 0]);
plot([0 0],[-2 2]); 
axis([-2 2 -2 2])
% plot corrected points
plot(corrPts(1:2*blockSize), 'r.')
plot(corrPts(2*blockSize:end), 'b.')



bitErrorRate = getBitErrorRate(transmPts, corrPts, points)

corr = xcorr(corrPts, transmPts);

Rxy = max(abs(corr));

k = Rxy/(sum(abs(transmPts.^2)));
w = abs(corrPts) - abs(transmPts)/k;

% signalPower = (norm(recdNoisyPts)^2)/length(recdNoisyPts);
% noisePower = (norm(w)^2)/length(w);
% P_x = sum(abs(transmPts.^2));
% P_w = sum(abs(w.^2));
% snr = 10*log10(signalPower/noisePower)
% Fc = 2.1e9;
% t = 1/16;
% v = real(recdNoisyPts).*cos(2*pi*Fc*t) - imag(recdNoisyPts).*sin(2*pi*Fc*t);
% % snr(v)
    