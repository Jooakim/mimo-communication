clear all
close all
clc 

% ------------ Variables ------------
% To set
blockSize = 100; 
nbrOfPilots = 10;
%samplingRate = 16;
nbrOfSimulations = 1e4;
points = [-1+1i -1-1i 1+1i 1-1i];

% Don't touch
nbrBitErr = 0;
corrPts = zeros(nbrOfSimulations, 1);
transmPts1 = zeros(nbrOfSimulations,1);
transmPts2 = zeros(nbrOfSimulations,1);
nbrOfBlocks = nbrOfSimulations/blockSize;

% ------------ Transmit Points ------------
rndIndexVector = ceil(length(points)*rand(nbrOfSimulations, 1));
for j = 1:nbrOfBlocks
    transmPts1((j-1)*blockSize+1:j*blockSize) = transmitBlock(points, rndIndexVector, nbrOfPilots, blockSize, j);
end
pts1 = channel(transmPts1, 0);
pts1 = resample(pts1, 1, 1);
pts1 = pts1;
recdPts1 = channel(pts1, 64);


% ------------ Transmit Points ------------
rndIndexVector = ceil(length(points)*rand(nbrOfSimulations, 1));
for j = 1:nbrOfBlocks
    transmPts2((j-1)*blockSize+1:j*blockSize) = transmitBlock(points, rndIndexVector, nbrOfPilots, blockSize, j);
end
pts2 = channel(transmPts2, 0);
pts2 = resample(pts2, 1, 1);

angle = pi/12;
recdPts2 = channel(pts2, 64);

corrPhase = estimateChannelAlt(conj((recdPts1)')+conj((recdPts2)'), conj(pts1'), conj(pts2'))/(2*pi )*360
corrAngleError = abs(corrPhase - angle/(2*pi)*360)

SNR = getSNR(recdPts1, pts1)



%%

clear all
clc


points = [-1+1i -1-1i 1+1i 1-1i];

blockSize = 100; 
nbrOfPilots = 10;
%samplingRate = 16;
nbrOfSimulations = 1e4;


nbrBitErr = 0;
corrPts = zeros(nbrOfSimulations, 1);
transmPts1 = zeros(nbrOfSimulations,1);
transmPts2 = zeros(nbrOfSimulations,1);
nbrOfBlocks = nbrOfSimulations/blockSize;

rndIndexVector = ceil(length(points)*rand(nbrOfSimulations, 1));
for j = 1:nbrOfBlocks
    transmPts((j-1)*blockSize+1:j*blockSize) = transmitBlock(points, rndIndexVector, nbrOfPilots, blockSize, j);
end
transmPts = conj(transmPts');

recdPts1 = channel(transmPts, 64 );
recdPts2 = channel(exp(1i*pi)*transmPts, 64+16+8);

[recdPts1, ~, pilotAngle1]= frequencyCorrection(recdPts1, transmPts, 20);
[recdPts2, ~, pilotAngle2] = frequencyCorrection(recdPts2, transmPts, 20);

channel1 = pinv(transmPts)*recdPts1;
channel2 = pinv(transmPts)*recdPts2;

corrAngle = angle(channel1) - angle(channel2)

abs(pilotAngle1 - pilotAngle2)