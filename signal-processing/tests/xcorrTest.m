x = randi(2,1,1000000);
pilots = x(100:400)+10;

signal = x;
pilotSeq  = zeros(1,length(signal));
for j = 1:length(signal)
    pilotSeq(j) = pilots(mod(j-1,length(pilots))+1);
end

corr = xcorr(signal, pilotSeq);

Rxy = max(abs(corr));

k = Rxy/(sum(abs(pilotSeq.^2)));
w = abs(signal) - k*abs(pilotSeq);
P_x = sum(abs(pilotSeq.^2));
P_w = sum(abs(w.^2));
snr = 10*log10(P_x/P_w);
