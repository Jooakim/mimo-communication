%Test code for continous synchronization with Gardner

clc
clear all

%To set
blockSize = 100; 
nbrOfPilots = 10;
samplingRate = 16;
nbrOfSimulations = 1e4;
points = [-1+1i -1-1i 1+1i 1-1i];

% Don't touch
nbrBitErr = 0;
transmPts = zeros(nbrOfSimulations,1);
nbrOfBlocks = nbrOfSimulations/blockSize;

rndIndexVector = ceil(length(points)*rand(nbrOfSimulations, 1));

% ------------ Transmit Points ------------
for k = 1:nbrOfBlocks
    transmPts((k-1)*blockSize+1:k*blockSize) = transmitBlock(points, rndIndexVector, nbrOfPilots, blockSize, k);
end
resampledPts = resample(transmPts, samplingRate, 1);

% ------------ Simulate errors ------------
% Modes turned on are specified by bitget on last param.
recdNoisyPts = channel(resampledPts, 64+32);

% ----------- Read into buffer ------------
nbrOfBuffers = 2;
bufferSize = nbrOfSimulations*samplingRate/nbrOfBuffers;
buffer = zeros(bufferSize, nbrOfBuffers);
for j = 1:nbrOfBuffers
    for k = 1:bufferSize
        buffer(k,j) = recdNoisyPts(j+k-1);
    end
end

% --------- Sample signal with Gardner------
samplingPt = 0;
errorSum = 0;
nbrOfErrors = 0;
corrPts = zeros(bufferSize/samplingRate, nbrOfBuffers);
for j = 1:nbrOfBuffers
    [sampledPts, samplingPt, errorSum, nbrOfErrors] = ...
        gardnerSync(buffer(:,j), samplingRate, samplingPt, errorSum, nbrOfErrors);
    corrPts(:,j) = sampledPts;
end

corrPts = corrPts(:);


bitErrorRate = getBitErrorRate(transmPts, corrPts, points)

figure
hold on
% plots coordinate system
plot([-2 2],[0 0]);
plot([0 0],[-2 2]); 
axis([-2 2 -2 2])

r = 0;
b = 0;
% plot corrected points
for j = 1:length(transmPts)
    [~,indexOfCorr] = min(abs(points-corrPts(j))); % Get index to closest point
    ncorrPts(j) = points(indexOfCorr); % Corrects recieved points
    if ncorrPts(j) == transmPts(j)
        plot(corrPts(j), 'r.')
        r = r + 1;
    else
        plot(corrPts(j), 'b.')
        b = b + 1;
    end
end




