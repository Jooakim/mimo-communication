N=1024; % Number of samples to generate
f=5; % Frequency of the sinewave
FS=200; % Sampling frequency
n=0:N-1; % Sampling index
x=sin(2*pi*f*n/FS); % Generate x(n)
%y=x+10*randn(1,N); % Generate y(n)
for i=1:10000
    clf
    y=sin(2*pi*f*n/FS+pi*i/100);
    subplot(3,1,1);
    plot(x);
    title('Pure Sinewave');
    grid;
    subplot(3,1,2);




    plot(y);
    title('y(n), Pure Sinewave + Noise');
    grid;
    [Rxy, lags]=xcorr(x,y); % Estimate the cross correlation

    Rxy = abs(Rxy);
    index = lags(find(Rxy == max(Rxy)))


    subplot(3,1,3);
    plot(lags,Rxy);
    title('Cross correlation Rxy');
    grid;
    pause(.1)
end