function [ sampledPts ] = samplingSync( signalPts, samplingRate )
%SAMPLINGSYNC Summary of this function goes here
%   Detailed explanation goes here
    
    samplePos = 2 ;
    latestSamplePos = samplePos;
    sampledPts = zeros(length(signalPts)/samplingRate,1);
    %sampledPts(1) = signalPts(1);
    errorPts = zeros(length(signalPts),1);
    for k = 3:length(signalPts)-2
        errorI = real(signalPts(k))*(real(signalPts(k - 2)) - real(signalPts(k + 2)));
        errorQ = imag(signalPts(k))*(imag(signalPts(k - 2)) - imag(signalPts(k + 2)));
        error = errorI + errorQ;
        errorPts(k) = error;
    end
    
    for k = 1:2
       latestError = 2;
       error = 1;
        while latestError*error >= 0 && abs(latestError) > abs(error)
            latestError = error;
            errorI = real(signalPts(samplePos))*(real(signalPts(samplePos - 1)) - real(signalPts(samplePos + 1)));
            errorQ = imag(signalPts(samplePos))*(imag(signalPts(samplePos - 1)) - imag(signalPts(samplePos + 1)));
            error = errorI + errorQ;
            
            if latestError*error >= 0 && abs(latestError) > abs(error)
                latestSamplePos = samplePos;
                if error < 0 
                    samplePos = samplePos + 1;
                else
                    samplePos = samplePos - 1;
                    if samplePos < 2
                        break;
                    end
                end
            end
        end
        
        if abs(latestError) < abs(error)
            sampledPts(k) = signalPts(latestSamplePos);
            latestSamplePos
            samplePos
        else
            sampledPts(k) = signalPts(samplePos);
                        latestSamplePos
            samplePos
        end
        samplePos = samplePos + samplingRate;
    end
    disp(errorPts);
    
end

