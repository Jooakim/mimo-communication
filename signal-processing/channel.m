function y=channel(x,mode)
% x is input (column vector)
% The bits in mode indicates which effects are turned on
% mode=0: nothing
% mode=1: IQ imbalance
% mode=2: Amplifier nonlinearity, max amplitude=1
% mode=4: Channel frequency selectivity 
% mode=8: Frequency offset 
% mode=16: Phase noise 
% mode=32: Sampling jitter, unknown delay
% mode=64: AWGN 20 dB
% For example, mode=23 gives sampling jitter, frequency offset, amplifier nonlinearity, IQ imbalance 

persistent phase

if size(phase,2)<1
    phase=0;
    disp('starting');
end
if size(x,2)>size(x,1)
    error('x must be a column vector');
end

y=x;

if bitget(mode,1) % I/Q imbalance
    'nothing yet'
end
if bitget(mode,2) % Amplifier nonlinearity
    y=2*tanh(abs(y)/2).*exp(1i*angle(y));
end
if bitget(mode,3) % Channel frequency selectivity
    Y=fft(y);
    f=linspace(0,2*pi*8,length(y))';
    Y=Y.*(1+0.5*sin(f));
    y=ifft(Y);
end
if bitget(mode,4) % Frequency offset
    t=(1:length(y))';
    y=y.*exp(1i*2*pi*t*0.0001);
end
if bitget(mode,5) % phase noise
    PN=cumsum(0.003*randn(size(y)));
    y=y.*exp(1i*(PN+phase));
    phase=phase+PN(end);
end
if bitget(mode,6) % Sampling jitter, unknown delay
    T=(1:length(y))';
    T=T.*1.001;
    y=sincinterpolate(y,T,32);
end
if bitget(mode,7) % AWGN
    N=10^(-20/10);
    y=y+sqrt(N)*(randn(size(y))+1i*randn(size(y)));
end
if bitget(mode,8) % Test
    y=filter([1 0.4 0.2],1,y);
    %y = y/(round(2*(rand(1) + 1)) + 1i);
end


