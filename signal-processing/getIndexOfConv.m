function indexOfConv = getConvPt(noiseVec, nbrSummedPts)
    
    % DON'T USE THE DATA IF DOESNT CONVERGE - RETURN -1?

    %convNoisePts = noiseVec(1:end); % Pts that probably has converged
    
    indexOfConv = 1;
    correntNoise = max(noiseVec);
    meanNoise = sum(noiseVec)/length(noiseVec);
    
    while correntNoise > meanNoise
        indexOfConv = indexOfConv + 1;
        correntNoise = sum(noiseVec(indexOfConv:indexOfConv+10))/10; % 10 to ensure convergence
    end
end

