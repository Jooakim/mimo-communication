function  blockPts = transmitBlock( points, rndIndexVector, nbrOfPilots, blockSize, dataIndex )
%TRANSMITBLOCK Summary of this function goes here
%   Detailed explanation goes here
    
    blockPts = zeros(blockSize, 1);
    
    dataIndex = (dataIndex-1)*blockSize + 1;

    for k = 1:nbrOfPilots
        blockPts(k) = points(mod(k-1, length(points))+1);
    end

    for k = nbrOfPilots+1:blockSize
        blockPts(k) = points(rndIndexVector(dataIndex));
        dataIndex = dataIndex + 1;
    end
        

end

