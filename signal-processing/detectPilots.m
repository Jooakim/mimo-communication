function [ index, signal, Rxy, lags, index2,count] = detectPilots( signal, refPilots )
%DETECTPILOTS Returns the index of the first pilot. Returns -1 if there are
%no pilots in signal.
%   Detailed explanation goes here
%     figure
    [Rxy, lags]=xcorr(signal,refPilots ); % Estimate the cross correlation
    Rxy = abs(Rxy);
    index = lags(find(Rxy == max(Rxy)));
    index = index(1);
    index2 = index;
    count = 0;
    while index < 0
        % index  = index + blocksize
        index = index + length(refPilots);
        count = count +1;
    end
    signal = signal(index+1:end);

end

