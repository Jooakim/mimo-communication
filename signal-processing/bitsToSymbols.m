function [ symbols ] = bitsToSymbols(bits)
%BITSTOSYMBOLS Given a 
%   Detailed explanation goes here

    indexOfBit = 1;
    
    points = [-1+1i -1-1i 1+1i 1-1i]/sqrt(2);
    symbols = zeros(length(bits)/2,1);

    for j = 1:length(bits)/2
        
        if bits(indexOfBit) == 1 && bits(indexOfBit+1) == 0
            indexOfPilot = 1;
        elseif bits(indexOfBit) == 1 && bits(indexOfBit+1) == 1
            indexOfPilot = 2;  
        elseif bits(indexOfBit) == 0 && bits(indexOfBit+1) == 0
            indexOfPilot = 3;
        else
            indexOfPilot = 4;
        end
        symbols(j) = points(indexOfPilot);
        indexOfBit = indexOfBit + 2;
    end

end