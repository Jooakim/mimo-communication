function plotEyeDiagram( points, samplingRate )
%PLOTEYEDIAGRAM plots an eye diagram for a resampled signal
%   
%   Plots a subarray from points with length equal to the 
%   samplingRate one at a time in the same figure until all 
%   points in the array are plotted.

    nbrOfEyes = 2;
    plotInterval = nbrOfEyes*samplingRate;

    hold on
    for i = 1:plotInterval:(length(points) - plotInterval)
        plot(0:plotInterval,imag(points(i:i+plotInterval)));
    end
    
    % Coordinate system
    plot([-.1*plotInterval 1.1*plotInterval],[0 0]);
    plot([0 0],[min(real(points))-.2 max(real(points))+.2]);
    hold off
    
    axis([-1 nbrOfEyes*samplingRate+1 min(real(points))-.2 max(real(points))+.2])   
end

