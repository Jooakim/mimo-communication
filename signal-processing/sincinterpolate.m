function y=sincinterpolate(x,t,N)
% Y = SINCINTERPOLATE(X,T,N)
% Computes the signal at arbitrary time positions T.
% X is assumed to have a sampling interval of 1, with the first entry in X at time 1 
% and the last at time length(X).
% T is a vector of new desired sampling instants.
% The elements of T should normally be an increasing sequence with values
% in the interval 1 <= T(k) < T(k+1) <= length(x).
% N is the window size. (default 64)
% (c) Thomas Eriksson 2007

if nargin<3
    N=64;   % The length of the interpolation is 2*N+1, see calc of p1 and p2 below.
end;
beta=0;
if size(x,1)<=1
    x=x.';
end
t=t(:);

y=zeros(length(t),size(x,2));
for k=1:length(t)
    p=round(t(k));
    p1=max(1,min(p-N,size(x,1)-1));
    p2=min(max(p+N,2),size(x,1));
    h=0.5*(1+cos(pi/(p2-p1)*((p1:p2)-t(k))));
    y(k,:)=(h.*sinc((p1:p2)-t(k)))*x(p1:p2,:);
end

end
