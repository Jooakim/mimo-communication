function [sampledPts, samplingPt, errorSum, nbrOfErrors] = ...
    gardnerSync(signalPts, samplingRate, samplingPt, errorSum, nbrOfErrors)
%GARDNERSYNC Summary of this function goes here
%   Detailed explanation goes here

     if samplingPt == -1
         samplingPt = round(samplingRate/2)+3;
     end
    
    lambda = 0.957;
    stepLength = samplingRate*.325;
    tolerance = 0.1;

    errorSum = 0;
    sampledPts = zeros(length(signalPts)/samplingRate, 1);
    nbrOfErrors = 0;
    errorVec = 0;
    
    % Set the first point as the first sample point
    sampledPts(1) = signalPts(1);
    for k = 2:length(signalPts)/samplingRate
        if samplingPt + samplingRate/2 < length(signalPts)
            % Calculate the error
            errorI = real(signalPts(samplingPt))*(real(signalPts(samplingPt - samplingRate/2)) - real(signalPts(samplingPt + samplingRate/2)));
            errorQ = imag(signalPts(samplingPt))*(imag(signalPts(samplingPt - samplingRate/2)) - imag(signalPts(samplingPt + samplingRate/2)));
            error = errorI + errorQ;

            % Calculate the error using small changes  
            errorSum = lambda*errorSum + (1-lambda)*error;
            nbrOfErrors = nbrOfErrors + 1;

            % Changes the sample timing with a 
            % rate related to the size of the error
            if nbrOfErrors == 30
                timingCorrection = -1*sign(errorSum)*round(abs(errorSum)*stepLength);
                if abs(errorSum) > tolerance && length(signalPts) > samplingPt + timingCorrection
                    samplingPt = samplingPt + timingCorrection;   
                end
                nbrOfErrors = 0;
                errorVec(end+1) = errorSum;
                errorSum = 0;
            end
            sampledPts(k) = signalPts(samplingPt);
            samplingPt = samplingPt + samplingRate;

            % Repetera
        elseif samplingPt <= length(signalPts)
            sampledPts(k) = signalPts(samplingPt);
            % To ensure correct sampling point on next function call
            samplingPt =  samplingPt + samplingRate;
        end
    end
         samplingPt = samplingPt + samplingRate - length(signalPts);
%         figure
%         plot(errorVec)
end

