function shortSNRVec = getNoiseVector(signal, pilots, nbrOfSumPts)
%GETSNR returns the signal to noise ratio 
    
    snrVec = zeros(1,length(signal));
    shortSNRVec = 0;
    pilotSeq  = zeros(1,length(signal));
    for point = 1:length(signal)
        pilotSeq(point) = pilots(mod(point-1,length(pilots))+1);
        snrVec(point) = abs(signal(point) - pilotSeq(point));
    end
    
    for i = 1:nbrOfSumPts:length(snrVec)
        if i+nbrOfSumPts <= length(snrVec)
            shortSNRVec(end+1) = sum(snrVec(i:i+nbrOfSumPts-1));
        else 
            shortSNRVec(end+1) = sum(snrVec(i:length(snrVec)));
        end
    end
    shortSNRVec = shortSNRVec./nbrOfSumPts;
    shortSNRVec = shortSNRVec(2:end);
    
%     snrVec = zeros(1,length(signal))
%     pilotSeq  = zeros(1,length(signal));
%     for j = 1:length(signal)
%         pilotSeq(j) = pilots(mod(j-1,length(pilots))+1);
%     end
%     
%     for point = 1:length(signal)
%         w = abs(signal(point) - pilotSeq(point));
%         P_x = abs(pilotSeq(point)^2);
%         P_w = abs(w^2);
%         
%         snrVec(point) = 10*log10(P_x/P_w);
%     end
end
