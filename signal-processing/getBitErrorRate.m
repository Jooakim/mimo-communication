function [ bitErrorRate ] = getBitErrorRate( transmPts, corrPts, points )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    nbrBitErr = 0;

    for j=1:length(transmPts)

        [~,indexOfCorr] = min(abs(points-corrPts(j))); % Get index to closest point
        corrPts(j) = points(indexOfCorr); % Corrects recieved points

        if (transmPts(j) ~= corrPts(j))
            nbrBitErr = nbrBitErr+1;
        end
    end

    bitErrorRate = nbrBitErr/length(transmPts);
    %10*log10(sum(abs((x.^2)))/sum(abs(x-y).^2))
%    10*log10(sum(abs((transmPts.^2)))/sum(abs(transmPts-corrPts).^2))
    
end

