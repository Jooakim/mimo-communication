function [ blockPts, meanCorrAngle, pilotAngle ] = frequencyCorrection( blockPts, refPts, nbrOfPilots)
%FREQUENCYCORRECTION Corrects the frequency offset and phase noise for a block of points

    % Intensity of the correction
    corrFract = 1e-4;
    
    % Variables
    meanCorrAngle = 0;
    
    % Correct a constant frequency offset
    if length(blockPts) > nbrOfPilots
        pilotAngle = angle(blockPts(nbrOfPilots)) - angle(refPts(end));
    else
        pilotAngle = angle(blockPts(1)) - angle(refPts(1));
    end
    blockPts = blockPts*exp(-1i*pilotAngle);
    
    % Correct phase noise
    for j = 1:length(blockPts)
        % Get index to closest pt
        [notImportant,indexOfCorr] = min(abs(refPts-blockPts(j)));
        % Calculate angle to closest pt from pt j
        corrAngle = angle(blockPts(j))-angle(refPts(indexOfCorr)); 
        
        % Calculate the mean phase-offset
        meanCorrAngle = meanCorrAngle + corrAngle;
        % Corrects all pts from pt j
        blockPts(j:end) = blockPts(j:end)*exp(-corrFract*1i*corrAngle); 
    end
    % Convert the mean value to degrees
    meanCorrAngle = 180/pi*meanCorrAngle/length(blockPts);
    pilotAngle = pilotAngle*180/pi;
end

