Code written for my Bachelor's thesis. The communication system was built in LabVIEW using multiple USRPs, radio devices. 
The signal processing was done in MATLAB and integrated within LabVIEW. The code was written in colaboration with two other people, but we were 
doing almost everything together since the hard part was figuring out how to do the signal processing rather than the actual coding.